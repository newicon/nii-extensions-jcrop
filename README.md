JCrop Nii Extension
===================

The JCrop API packaged for Nii

This extension compromises 3 main parts:

* The JCrop API javascript source code
* Two CApplicationComponents: JCrop and JCropper
* An implementaion of CAction, JCropAction, which processes images and performs the actual cropping

### Requirements

**To use the JCropper component, the PHP GD image processing library must be installed.**

Components
----------

The extension has two components: one for simply registering the required client scripts, and one to 
perform actual image cropping requests.

### JCrop

The JCrop component is used to registered the required clientside javascript for JCrop

```php
Yii::app()->setComponents('jcrop' => array('class' => 'niiextensions.jcrop.components.JCrop'));
Yii::app()->jcrop->registerScripts();
```

### JCropper


Actions
-------

The extension comes with an action, JCropAction, which can be registered by controllers to provide crop processing capabilities:

```php
/// MyController.php
class MyController extends CController
{
	.........

        public function actions()
        {
                return array(
                        'crop' => array(
                                'class'         => 'niiextensions.jcrop.actions.JCropAction',
                        ),
                );
        }
}
```

The action expects to recieve $_POST parameters of the following:

* image_id - The NFile ID of the image to be processed
* x        - x coordinate of crop origin 
* y        - y coordinate of crop origin
* w        - width of crop
* h        - height of crop


