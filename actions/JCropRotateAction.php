<?php
class JCropRotateAction extends CAction
{
	public $overwrite = false;
	
	public function onRotate($event)
	{
		$this->raiseEvent('onRotate',$event);
	}

	public function run()
	{
		Yii::import('niiextensions.jcrop.components.JCropper');
		$jcropper = new JCropper();

		$image_id = intval($_POST['image_id']);
		$imagePath = NFileManager::get()->getFilePath($image_id);

		$rotation = $jcropper->getRotationFromPost();
		$newPath = $jcropper->rotate($imagePath,$rotation);

		if($this->overwrite)
		{
			file_put_contents($imagePath, file_get_contents($newPath));
			unlink($newPath);
			$file = NFileManager::get()->getFile($image_id);
		}
		else
		{
			$file = NFile::addNewFile('',basename($newPath),basename($newPath),filesize($newPath),mime_content_type($newPath),'nii');
		}

		$model_id 		= Yii::app()->getRequest()->getQuery('model_id',null);
		$model_class 	= Yii::app()->getRequest()->getQuery('model_class',null);
		$attribute 		= Yii::app()->getRequest()->getQuery('attribute',null);

		$data = array('file' 		=> $file,
					  'file_id'		=> $file->id, 
			          'url' 		=> NFileManager::get()->getUrl($file),
			          'model_id'	=> $model_id,
			          'model_class'	=> $model_class,
			          'attribute'	=> $attribute);

		echo CJSON::encode($data);

		// Raise an event for controllers to get
        $event = new CEvent($this, $data);
        $this->onRotate($event);

		return true;
	}
}
