function ncrop_update(coords,id)
{
	$('#'+id+'_x').val(coords.x);
	$('#'+id+'_y').val(coords.y);
	$('#'+id+'_x2').val(coords.x2);
	$('#'+id+'_y2').val(coords.y2);
	$('#'+id+'_w').val(coords.w);
	$('#'+id+'_h').val(coords.h);
}

function ncrop_initBasic(id,options)
{
	options.onChange = function(c){
		ncrop_update(c,id);
	};
	$('#'+id).Jcrop(options);
}

function ncrop_init(id,options)
{
	var jcrop = {};
	options.onChange = function(c){
		ncrop_update(c,id);
	};
	function doAjaxRequest(id)
	{
		var data = {};
		var d = ['x','y','x2','y2','h','w'];
		for(var i = 0; i < d.length; i++)
			data[id+'_'+d[i]] = $('#'+id+'_'+d[i]).val();

		//for(var o in options.ajaxParams)
		//	data[o] = options.ajaxParams[o];

		$.ajax({
			url:  options.ajaxUrl, 
			data: data, 
			type: 'POST',
			success: function(data){
				alert("POSTED!");
			}
		});
	}



	$('body').on('click','#start_'+id, function(event){
		$(this).hide();
		$('#crop_'+id+',#cancel_'+id).show();

		if(!jcrop.id)
		{
			jcrop.id = $.Jcrop('#'+id,options);
		}
		jcrop.id.enable();
		var dim = jcrop.id.getBounds();
		jcrop.id.animateTo([dim[0],4],[dim[1]/4],[dim[0]/2],[dim[1]/2]);
	});

	$('body').on('click','#crop_'+id, function(event){
		$('#start_'+id).show();
		$('#crop_'+id+', #cancel_'+id).hide();
		doAjaxRequest(id);
		jcrop.id.release();
		jcrop.id.disable();
	});

	$('body').on('click','#cancel_'+id,function(event){
		$('#start_'+id).show();
		$('#crop_'+id+', #cancel_'+id).hide();
		jcrop.id.release();
	});

}