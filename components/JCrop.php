<?php

class JCrop extends CApplicationComponent
{
	protected $_assetsUrl;

	public function registerScripts()
	{
		$assetsUrl = $this->getAssetsUrl();
		$clientScript = Yii::app()->getClientScript();
		$clientScript->registerScriptFile($assetsUrl . '/js/jquery.Jcrop.min.js');
		$clientScript->registerScriptFile($assetsUrl . '/js/ncrop.js');
		$clientScript->registerCssFile($assetsUrl . '/css/jquery.Jcrop.min.css');
	}

	public function getAssetsUrl()
	{
		if($this->_assetsUrl !== null)
			return $this->_assetsUrl;

		$assetsPath = Yii::getPathOfAlias('niiextensions.jcrop.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		return $this->_assetsUrl = $assetsUrl;
	}
}
