<?php

class JCropper
{
	public $jpeg_quality	= 200;
	public $png_compression = 2;

	public $cropped_width 	= 200;
	public $cropped_height 	= 200;

	public $croppedDir;

	public function crop($src,$coords)
	{
		/* Use DG to create our image based on the type */
		$img = $this->getImage($src);

		/* Create the new name for image */
		$croppedPath = $this->getPath($src);

		$this->cropped_width = $coords['x2'] - $coords['x'];
		$this->cropped_height = $coords['y2'] - $coords['y'];

		/* Create our new image */
		$croppedImage = imagecreatetruecolor($coords['w'], $coords['h']);

		if(!imagecopyresampled($croppedImage, $img, 0, 0, $coords['x'], $coords['y'], $coords['w'], $coords['h'], $coords['w'], $coords['h']))
			return false;

		$this->saveImage($src,$croppedImage,$croppedPath);
		return $croppedPath;
	}

	public function rotate($src,$rotation)
	{
		$rotatedPath = $this->getPath($src);
		$img = $this->getImage($src);

		$rotatedImage = imagerotate($img, $rotation, 0);

		$this->saveImage($src,$rotatedImage,$src);
		return $src;
	}

	private function getImage($src)
	{
		$type = strtolower(pathinfo($src,PATHINFO_EXTENSION));
		switch( $type )
		{
			case 'jpg':
			case 'jpeg':
				$img = imagecreatefromjpeg($src);
				break;
			case 'gif':
				$img = imagecreatefromgif($src);
				break;
			case 'png':
				$img = imagecreatefrompng($src);
				break;

			default:
				return false;
		}
		return $img; 
	}

	private function saveImage($src,$image,$path)
	{
		switch(strtolower(pathinfo($src,PATHINFO_EXTENSION)))
		{
			case 'jpg':
			case 'jpeg':
				imagejpeg($image,$path,$this->jpeg_quality);
				break;
			case 'gif':
				imagegif($image,$path);
				break;
			case 'png':
				imagepng($image,$path,$this->png_compression);
				break;
		}
	}


	private function getPath($src)
	{
		/* Create the new name for image */
		if(!$this->croppedDir)
			$this->croppedDir = dirname($src);
		
		$type = strtolower(pathinfo($src,PATHINFO_EXTENSION));
		$srcFilenameWithoutExtension = basename($src,$type);
		$croppedFileName = $srcFilenameWithoutExtension . '_' . $this->cropped_width . '_' . $this->cropped_height . '.' . $type;

		$croppedPath = $this->croppedDir . DIRECTORY_SEPARATOR . $croppedFileName;
		return $croppedPath;
	}

	public function getCoordsFromPost()
	{
		$coords = array('x' => null, 'y' => null, 'h' => null, 'w' => null, 'x2' => null, 'y2' => null);
		foreach ($coords as $key => $value) {
			$coords[$key] = $_POST[$key];
		}
		return $coords;
	}

	public function getRotationFromPost()
	{
		return $_POST['rotation'];
	}
}
?>
